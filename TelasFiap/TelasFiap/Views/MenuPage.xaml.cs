﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace TelasFiap.Views
{
    public partial class MenuPage : ContentPage
    {

        public ListView ListView { get { return listView; } }

        public MenuPage()
        {
            InitializeComponent();

            // Criando a lista de Opções de Menu
            // E adicionando ao ListView
            var itemsMenu = new List<MenuItem>();
            itemsMenu.Add(new MenuItem
            {
                Titulo = "Config",
                Icone = "config.png",
                TargetType = typeof(Views.ConfigPage)
            });

            itemsMenu.Add(new MenuItem
            {
                Titulo = "Perfil",
                Icone = "perfil.png",
                TargetType = typeof(Views.PerfilPage)
            });

            itemsMenu.Add(new MenuItem
            {
                Titulo = "Ajuda",
                Icone = "ajuda.png",
                TargetType = typeof(Views.AjudaPage)
            });

            itemsMenu.Add(new MenuItem
            {
                Titulo = "Tabbed Page",
                Icone = "ajuda.png",
                TargetType = typeof(Views.MenuTabbedPage)
            });

            listView.ItemsSource = itemsMenu;


        }
    }
}
