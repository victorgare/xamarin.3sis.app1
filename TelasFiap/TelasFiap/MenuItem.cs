﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TelasFiap
{
    public class MenuItem
    {

        // Titulo
        public string Titulo { get; set; }

        // IconSource
        public string Icone { get; set; }

        // Target
        public Type TargetType { get; set; }
    }
}
